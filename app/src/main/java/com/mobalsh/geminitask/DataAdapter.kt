package com.mobalsh.geminitask

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView.*
import com.mobalsh.geminitask.databinding.RecyclerItemDataBinding
import com.mobalsh.geminitask.model.DataModel

class DataAdapter : Adapter<DataAdapter.DataViewHolder>() {
    private val dataItemsList = arrayListOf<DataModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val binding: RecyclerItemDataBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.recycler_item_data, parent, false
        )
        return DataViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bind(dataItemsList[position])
    }

    override fun getItemCount() = dataItemsList.size

    fun setData(newData: ArrayList<DataModel>) {
        dataItemsList.addAll(newData)
        notifyItemRangeChanged(0, dataItemsList.size)
    }

    inner class DataViewHolder(private val binding: RecyclerItemDataBinding) :
        ViewHolder(binding.root) {
        fun bind(dataModel: DataModel) {
            binding.dataModel = dataModel
        }
    }
}