package com.mobalsh.geminitask

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mobalsh.geminitask.model.DataModel
import com.mobalsh.geminitask.networking.repos.DataRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DataViewModel(
    private val dataRepo: DataRepo
) : ViewModel() {
    var data: MutableLiveData<ArrayList<DataModel>> = MutableLiveData()

    init {
        loadData()
    }

    private fun loadData() {
        CoroutineScope(Dispatchers.Main).launch {
            data.value = dataRepo.getData().responseBody?.data!!
        }
    }
}