package com.mobalsh.geminitask.model.bases

import com.google.gson.Gson

interface BaseItem

fun BaseItem.toJson(): String = Gson().toJson(this)

inline fun <reified T : BaseItem> String.fromJson(): T = Gson().fromJson(this, T::class.java)
