package com.mobalsh.geminitask.model.bases

import java.io.Serializable

data class BaseResponse<T>(
    var status: String = "",
    var message: String = "",
    var code: Int = 0,
    val data: T? = null,
) : Serializable, BaseItem