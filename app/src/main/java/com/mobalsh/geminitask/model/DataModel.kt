package com.mobalsh.geminitask.model

import java.io.Serializable

data class DataModel(
    val id: Int? = null,
    val email: String? = null,
    val first_name: String? = null,
    val last_name: String? = null,
    val avatar: String? = null
) : Serializable