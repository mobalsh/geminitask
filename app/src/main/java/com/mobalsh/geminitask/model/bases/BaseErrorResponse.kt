package com.mobalsh.geminitask.model.bases

import java.io.Serializable

data class BaseErrorResponse(
    var status: String = "",
    var message: String = "",
    var code: Int = 0
) : Serializable