package com.mobalsh.geminitask.model.bases

data class NetworkOutcome<T>(val isRequestSuccess: Boolean,
                             val responseBody: T?,
                             val errorResponse: BaseErrorResponse
)