package com.mobalsh.geminitask.model

import java.io.Serializable

data class DataResponse(
    val page: Int? = null,
    val per_page: Int? = null,
    val total: Int? = null,
    val total_pages: Int? = null,
    val data: ArrayList<DataModel>? = null
) : Serializable