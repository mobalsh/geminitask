package com.mobalsh.geminitask.networking.networkresponses

import android.util.Log
import com.mobalsh.geminitask.exception.NetworkExceptionHandler
import com.mobalsh.geminitask.model.bases.BaseResponse
import com.mobalsh.geminitask.model.bases.NetworkOutcome
import com.google.gson.Gson
import com.mobalsh.geminitask.exception.ResponseErrorException
import com.mobalsh.geminitask.model.bases.BaseErrorResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

suspend fun <T> Call<T>.getNetworkResponse(): NetworkOutcome<T> = suspendCoroutine {
    enqueue(object : Callback<T> {
        override fun onResponse(call: Call<T>?, response: Response<T>) {
            if (response.isSuccessful) {
                it.resume(NetworkOutcome(true, response.body(), BaseErrorResponse()))
            } else {
                val errorModel: BaseErrorResponse =
                    Gson().fromJson(response.errorBody()!!.string(), BaseErrorResponse::class.java)
                Log.e("ERROR", response.errorBody()!!.string())
                when (response.code()) {
                    400, 404, 401, 409, 405 -> it.resumeWithException(
                        ResponseErrorException(errorModel)
                    )
                    else -> it.resumeWithException(ResponseErrorException(errorModel))
                }
            }
        }

        override fun onFailure(call: Call<T>, t: Throwable) {
            Log.e("ERROR", t.message!!)
            it.resumeWithException(NetworkExceptionHandler(t).getFailureException())
        }
    })
}
