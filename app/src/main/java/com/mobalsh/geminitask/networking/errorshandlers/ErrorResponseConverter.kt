package com.mobalsh.geminitask.networking.errorshandlers

import android.util.Log
import com.mobalsh.geminitask.model.bases.BaseErrorResponse
import com.mobalsh.geminitask.model.bases.BaseResponse
import com.mobalsh.geminitask.networking.networkclients.NetworkApiClient
import okhttp3.ResponseBody
import java.io.IOException

object ErrorResponseConverter {
    fun parseError(errorBody: ResponseBody): BaseErrorResponse = try {
        NetworkApiClient.apiClient.responseBodyConverter<BaseErrorResponse>(
            BaseResponse::class.java, arrayOfNulls(0)
        ).convert(errorBody)
            ?: BaseErrorResponse()
    } catch (e: IOException) {
        Log.e("ASASD", e.message!!)
        BaseErrorResponse()
    }
}
