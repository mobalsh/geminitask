package com.mobalsh.geminitask.networking.networkcallsImpl

import com.mobalsh.geminitask.networking.networkclients.NetworkApiClient
import com.mobalsh.geminitask.networking.networkresponses.getNetworkResponse
import com.mobalsh.geminitask.networking.endpoint.Data
import com.mobalsh.geminitask.networking.repos.DataRepo

class DataRepoImpl : DataRepo {

    private val data: Data by lazy {
        NetworkApiClient.apiClient.create(Data::class.java)
    }

    override suspend fun getData()=data.getData().getNetworkResponse()
}
