package com.mobalsh.geminitask.networking.repos

import com.mobalsh.geminitask.model.DataModel
import com.mobalsh.geminitask.model.bases.BaseResponse
import com.mobalsh.geminitask.model.bases.NetworkOutcome

interface DataRepo {
    suspend fun getData(): NetworkOutcome<BaseResponse<ArrayList<DataModel>>>
}