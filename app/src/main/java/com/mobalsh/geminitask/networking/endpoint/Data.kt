package com.mobalsh.geminitask.networking.endpoint

import com.mobalsh.geminitask.model.DataModel
import com.mobalsh.geminitask.model.bases.BaseResponse
import retrofit2.Call
import retrofit2.http.*

interface Data {
    @GET("users")
    fun getData(@Query("page") page: Int = 1): Call<BaseResponse<ArrayList<DataModel>>>
}