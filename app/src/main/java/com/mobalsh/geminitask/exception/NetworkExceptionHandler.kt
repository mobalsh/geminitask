package com.mobalsh.geminitask.exception

import com.mobalsh.geminitask.model.bases.BaseErrorResponse
import java.net.ConnectException
import java.net.NoRouteToHostException
import java.net.UnknownHostException

class NetworkExceptionHandler(private val throwable: Throwable) {
    fun getFailureException(): ResponseErrorException =
        when (throwable) {
            is UnknownHostException, is NoRouteToHostException -> ResponseErrorException(
                BaseErrorResponse(message = "NO Internet Now")
            )
            is ConnectException -> ResponseErrorException(BaseErrorResponse(message = "No Connection here"))
            else -> ResponseErrorException(BaseErrorResponse(message = "Response Error Exception"))
        }
}