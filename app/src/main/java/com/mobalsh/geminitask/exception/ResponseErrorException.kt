package com.mobalsh.geminitask.exception

import com.mobalsh.geminitask.model.bases.BaseErrorResponse


class ResponseErrorException(val errorModel: BaseErrorResponse) : Exception()
