package com.mobalsh.geminitask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.mobalsh.geminitask.databinding.ActivityMainBinding
import com.mobalsh.geminitask.networking.networkcallsImpl.DataRepoImpl
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val dataBindingView by lazy {
        DataBindingUtil.setContentView(this, R.layout.activity_main) as ActivityMainBinding
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModel = DataViewModel(dataRepo = DataRepoImpl())
        dataBindingView.viewModel = viewModel

        val dataAdapter = DataAdapter()
        rvMainData.adapter = dataAdapter

        viewModel.data.observe(this) {
            it.apply { dataAdapter.setData(it) }
        }
    }
}